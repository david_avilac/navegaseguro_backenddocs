## NAVEGA SEGURO DOCS 🧭🚢🌊
---

<div style="text-align:center;">

![Node.js](https://img.shields.io/badge/-Node.js-333333?style=flat&logo=node.js)
![Express.js](https://img.shields.io/badge/-Express.js-333333?style=flat&logo=express)
![Microsoft SQL Server](https://img.shields.io/badge/-Microsoft%20SQL%20Server-333333?style=flat&logo=microsoft-sql-server)
![Docker](https://img.shields.io/badge/-Docker-333333?style=flat&logo=docker)

</div>
Durante el proceso de desarrollo, se han incorporado diversas funcionalidades a la aplicación Navega Seguro. Una de ellas es el análisis del comportamiento del mar, lo cual permite evaluar las condiciones y tendencias del oleaje. Además, se ha incluido la medición de la sensación térmica, el nivel del mar, la presión atmosférica y la precipitación en la zona de interés. Estas funcionalidades amplían la capacidad de la aplicación y proporcionan a los usuarios información detallada sobre las condiciones ambientales en el entorno marino.
</br>
</br>


Durante el proceso de desarrollo de la aplicación, los estudiantes de la Universidad Católica de Colombia han incorporado diversas funcionalidades. Entre ellas se encuentra el análisis del comportamiento del mar, permitiendo evaluar las condiciones y tendencias del oleaje. Asimismo, se ha incorporado la medición de la sensación térmica, el nivel del mar, la presión atmosférica y la precipitación en la zona de interés. Estas funcionalidades amplían el alcance de la aplicación y brindan a los usuarios información detallada sobre las condiciones ambientales en el entorno marino.

# Arquitectura

Navega Seguro utiliza Node.js y una base de datos SQL Server, sigue la siguiente estructura de carpetas y archivos:

## Carpeta "src" (fuente):

### Carpeta "models"
Aquí se ubicarán los modelos de datos que representan las entidades del sistema. Cada modelo definirá la estructura y las relaciones de las tablas de la base de datos, en esta carpeta se realizan las peticiones de los datos.

```javascript
async function getAllCategoryUser(){
    try{
        let pool = await validatePool();
        let categoryUser = await pool.request()
            .query('Select * from categoria_activa')
        return categoryUser.recordsets;

    }catch(error){
        console.log(error);
    }
}

```

### Carpeta "routes" 
Aquí se encuentran los controladores, estos se encargarán de procesar las solicitudes HTTP recibidas, interactuar con los modelos de datos y enviar las respuestas adecuadas. Las rutas definirán las URL y los métodos HTTP correspondientes a cada endpoint del sistema.

```javascript
router.post('/getUnitMeasurement', (req,res) => {
    unit_measurement.getUnitMeasurement(req.body.variable).then(result=>{
        res.json(result[0]);
    })
});

module.exports = router;

```

### Archivo "config":
Aquí se colocarán los archivos de configuración relacionados con la conexión a la base de datos SQL Server y otras configuraciones específicas del proyecto.
Al requerir algun dato que se encuentre en este archivo se solicita por medio de la funcion `getConfig()`, esta retorna los valores almacenados en el archvivo.

```javascript 
function getVariableData(variable, unit, value) {
    let valueVariable = { "code": "2", "status": "fail", "message": "Data not found" };
    switch (variable.toLowerCase().replace(" ", "_")) {

        case getConfig().direccion_viento.name:
            valueVariable = decisionEngine.getDirectionWind(unit,value);
            break;
        case getConfig().velocidad_viento.name:
            valueVariable = decisionEngine.getSpeedWind(unit, value);
            break;
        case getConfig().precipitacion.name:
            valueVariable = decisionEngine.getPrecipitation(unit,value);
            break;
        case getConfig().sensacion_termica.name:
            valueVariable = decisionEngine.getThermalSensation(unit, value[0], value[1]);
            break;
        case getConfig().presion_atmosferica.name:
            valueVariable = decisionEngine.getAtmosphericPressure(unit,value);
            break;
    }
    valueVariable = [valueVariable.information.value, valueVariable.information.status, valueVariable.information.description, valueVariable.information.color]
    return valueVariable;
}

```
Es importante mencionar que la implementación precisa de la función `getConfig()` dependerá del formato y estructura del archivo de configuración utilizado, así como de las convenciones y preferencias del proyecto.


### Archivo  "index.js" 
Este archivo es el punto de entrada principal de la aplicación, donde se configura el servidor HTTP, se establecen las rutas y se realiza otras configuraciones generales.

### Carpeta "database" (base de datos):

Aquí se ubicaran los archivos y scripts relacionados con la base de datos SQL Server, como la creación de tablas.


# Variables de Entorno

En Navega Seguro, es esencial utilizar variables de entorno para almacenar contraseñas, usuarios y otros datos confidenciales. Para ello, se recomienda crear un archivo .env en el que se incluyan los valores necesarios para el correcto funcionamiento de la aplicación. 

A continuación, se muestra un ejemplo de los datos que deben ser almacenados en dicho archivo (los valores deben ser configurados según las necesidades del usuario):


```bash

### Credenciales bases de datos NAVEGA SEGURO

DB_NS_DB_USER=sa
DB_NS_DB_PASSWORD=root
DB_NS_DB_SERVER=localhost
DB_NS_DB_DATABASE=navegacionsegura
DB_NS_DB_PORT=1433

### Credenciales bases de datos DIMAR

DB_DIMAR_DB_USER=sa
DB_DIMAR_DB_PASSWORD=root
DB_DIMAR_DB_SERVER=localhost
DB_DIMAR_DB_DATABASE=pruebadimar
DB_DIMAR_DB_PORT=1433

### Credenciales bases de datos HISTORICO DIMAR

HR_DB_USER=basededatos3
HR_DB_PASSWORD=12345
HR_DB_SERVER=localhost
HR_DB_DATABASE=pruebadimar
HR_DB_PORT=1433

### Credenciales Servidor SMTP

SMTP_HOST=smtp.sendinblue.com
SMTP_PORT=587
SMTP_SECURE=fa
SMTP_USER=coreoregistrado@gmail.com
SMTP_PASS=B0YXkWvI5R1JZUqmsad
SMTP_TLS_REJECTUNAUTHORIZED=trye

### Credenciales API KEY

API_KEY_UV=d6020fa5c9a0f15443a2b27946
API_KEY_AIR=e79d1e39002a8686a8a6da428f4

### Credenciales host

DM_HOST=http:\\localhost:3200
```





## Bases de datos 

### Connection Pool
Un pool de conexiones es una colección de conexiones de bases de datos preestablecidas que se pueden reutilizar para mejorar la eficiencia y el rendimiento de una aplicación.

En lugar de crear una nueva conexión a la base de datos cada vez que se necesita acceder a ella, el pool de conexiones mantiene una serie de conexiones ya establecidas y listas para su uso. Cuando una aplicación necesita acceder a la base de datos, solicita una conexión del pool, la utiliza y luego la devuelve al pool para que pueda ser reutilizada por otra solicitud en el futuro.


Navega Seguro cuenta con conexion a diferentes funtes de datos, se tienen las siguientes bases de datos las cuales ayudan al funcionamiento del aplicativo.
 
 - NAVEGASEGURO
 - DIMAR I
 - HISTORICO_DIMAR

Para realizar solicitudes en el aplicativo Navega Seguro, se deben utilizar las siguientes funciones:
```javascript
  ValidePool(); //NAVEGASEGURO
  ValidatePoolDimar(); //DIMAR
  ValidatePoolDimarHR(); //HISTORICO_DIMAR 
```

Las funciones de tipo validatePool valida la conexión actual de la base de datos, pemitiendo interactuar con las mismas para realizar las solicitudes correspondientes. Es importante tener en cuenta que se debe asegurar la correcta conexión a la base de datos. revisar (VARIABLES DE ENTORNO).

```python
async function restartConnection() {
  try {
    await poolManagement.closePool("app");
    console.log("\x1b[31m%s\x1b[0m", "Restarting DB NAVEGA SEGURO...");
    newPool = poolManagement.createPool(getValues(), "app");
    return newPool;
  } catch (error) {
    console.error(error);
    throw error;
  }
}
```

Esto es necesario por la implementacion de funciones como restartConnection(), que se encarga de reiniciar la conexión con la base de datos de Navega Seguro. Esta función utiliza las funciones closePool() y createPool() de la variable poolManagement para cerrar y crear una nueva conexión con la base de datos, respectivamente.

```python
function validatePool() {
  if (newPool !== pool) {
    pool = newPool;
  }
  return pool;
}
```

### Peticiones a la base de datos.

Ejemplo para hacer peticiones a las bases de datos:

código es una función llamada getAllImages que obtiene todas las imágenes de la tabla dt_imagen en la base de datos de Navega Seguro.

```javascript
var { validatePool } = require("../../database/dbconfig_app");

async function getAllImages() {
  try {
    let pool = await validatePool();
    let images = await pool.request().query("SELECT * FROM admin_schema.dt_imagen;");
    return images.recordsets;
  } catch (error) {
    console.log(error);
  }
}
```

En este ejemplo, se llama a validatePool() para obtener un objeto de conexión a la base de datos. A continuación, se ejecuta una consulta SQL utilizando el objeto de conexión pool, que obtiene todas las imágenes de la tabla dt_imagen. Los resultados de la consulta se devuelven como un conjunto de registros.


## utilizando inputs
Se establece el parámetro `id_configuracion` en la consulta, utilizando el método `.input()` para especificar el tipo y el valor del parámetro. En este caso, se utiliza sql.Int para indicar que el parámetro es un entero y se asigna el valor de id_configuration.
```java
async function getLanguageConfigured(id_configuration) {
    try {
        let pool = await validatePool()
        let language = await pool.request()
            .input('id_configuracion', sql.Int, id_configuration)
            .execute('consultar_idioma_configurado')
        return language.recordsets;

    } catch (error) {
        console.log(error);
    }
}
```
## Cambiar credenciales de la base de datos desde el Módulo de Administración

Se realiza un ejemplo con el fin de entender el registro de una nueva base de datos.

Al momento de crear nuevas bases de datos en Navega Seguro, se debe seguir la estructura de nomenclatura establecida, la cual es ${prefix}_VARIABLE_DE_ENTORNO. Como ejemplo se agregara una nueva base de datos llamada "EJEMPLO" con el prefijo "EJ", el nombre de usuario de la base de datos debe ser almacenado en la variable EJ_DB_USER.

En el modulo "database.js" se importan los archivos donde se contienen las bases de datos, junto a su funcion para reiniciar la conexión.

```javascript
const {
  validatePool,
  restartConnection,
} = require("../../database/dbconfig_app");

const {
  restartConnectionDimar,
  validatePoolDimar,
} = require("../../database/dbconfig_dimar");

const {
  restartConnectionDimarHR,
  validatePoolDimarHR,
} = require("../../database/dbconfig_historico_redmpomm");

const {
  restartConnectionEjemplo,
  validatePoolDimarEJ,
} = require("../../database/dbconfig_ejemplo");

```

Previamente se debe crear el archivo donde se define la configuracion para la nueva base de datos.


Se debe definir un arreglo el cual se lista las tablas requeridas para el funcionamiento de la base de datos.

```javascript
const QUERIESTOVALIDATEEJ = [
  //"SELECT * FROM estaciones_disponibles",
  "SELECT * FROM dt_api",
  "SELECT * FROM dt_correo",
  "SELECT * FROM dt_imagen",
  "SELECT * FROM dt_database",
  "SELECT * FROM dt_servidor_smtp",
  "SELECT * FROM estacion",
  "SELECT * FROM notificacion",
  "SELECT * FROM usuario",
  "SELECT * FROM variable_uv",
  "SELECT * FROM variable",
];
```

Si se agrega una nueva base de datps se debe adicionar en la funcion, esto para que el módulo de administracion logre identificar las distinas bases de datos.

```javascript
if (res.id == 1) {
    setEnvVariables("DB_NS", res);
    restartConnection();
  } else if (res.id == 2) {
    setEnvVariables("DB_DIMAR", res);
    restartConnectionDimar();
  } else if (res.id == 3) {
    setEnvVariables("HR", res);
    restartConnectionDimarHR();
  } else if (res.id == 4) {
    setEnvVariables("EJ", res);
    restartConnectionEjemploEJ():
  } else {
    console.error("Base de datos no registrada.");
  }

```



## Autorización

---

Para poder realizar peticiones en la API y que está retorne información, es necesario proveer un API key en el header `authorization`.
Si no se tiene un API key, se puede generar uno en el siguiente End-point:

```http
POST /api/app/initial
```

Se recomienda el uso de Postman para la realización de las peticiones:

- Se crea una "Nueva Solicitud" y selecciona "POST".
- Ingrese la siguiente URL

```http
http://localhost:3000/api/app/initial/
```

- Click en "Body" y selecciona "raw" ademas, se hace uso de "JSON (application/json)" como formato.
- Escriba siguiente JSON en el cuerpo de la solicitud:

```json
{
  "source": "app"
}
```

Ejemplo:

![](https://https://gitlab.com/javelandia/dimar-backend-main/src/documentation/images/peticion_en_postman.png)

Respuesta esperada (API KEY para poder realizar peticiones al backend)

![](https://https://gitlab.com/javelandia/dimar-backend-main/src/documentation/images/ejemplo_api_key.png)

## Autorización II

---

Para inspeccionar la respuesta en Postman se necesita la clave `authorization` generada anteriormente:

- Se crea una "Nueva solicitud" y selecciona "GET".
- Para este ejemplo, se hace uso del endpoint que retorna los grados de escolaridad.

```http
GET http://localhost:3000/api/school_grade/getAllSchoolGrade
```

- En el apartado de "Authorization", se agrega un nuevo encabezado `type: API Key` con la siguiente información:

| Key    | `authorization`                                                    |
| ------ | ------------------------------------------------------------------ |
| Valor  | `Bearer <API_KEY>" (reemplaza <API_KEY> con tu clave de API real)` |
| Add to | `Header`                                                           |

Ejemplo

![](https://https://gitlab.com/javelandia/dimar-backend-main/src/documentation/images/peticion_get.png)

- Al enviar la solicitud se espera la siguiente respuesta.

![](https://https://gitlab.com/javelandia/dimar-backend-main/src/documentation/images/respuesta_get.png)






## Instalación

_Proyecto:_

```bash
git clone https://github.com/jscalderon65/maritimo-backend.git
cd maritimo-backend
npm install
```

_Base de datos:_

```bash
sudo docker run --name sql_server -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=P@ssw0rd1' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest
```

## Iniciando la aplicación

```bash
npm run dev
```
